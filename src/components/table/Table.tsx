import cn from 'classnames'
import * as React from 'react'
import {
    createRef,
    RefObject
} from 'react'
import { withRouter } from 'react-router'
import {
    compose,
    withProps,
    withState
} from 'recompose';
import styles from './table.module.css'

export interface ITable {
    fields?: Array<{
        label: string,
        type?: string,
        options?: any,
    }>,
    rows?: Array<{
        key: string,
        values: {
            [key: string]: any,
        },
    }>,
    onChangeField?(row: number, field: string, value: string): void,
    onKeyPress?(event: KeyboardEvent): void,
}

interface IConnectedTable extends ITable {
    activeRow?: number,
    containerRef?: RefObject<HTMLDivElement>,
    history?: any,
    setActiveRow?(value: number | null): number,
    onCellClick?(row: number, field: number): () => void,
    onFieldChange?(row: number, field: string): (event: {target: HTMLInputElement} & Event) => void,
}

const TableComponent = ({
    fields,
    rows,
    activeRow,
    onCellClick,
    onFieldChange,
    onKeyPress,
    history,
    containerRef,
}: IConnectedTable) => (
    <div ref={containerRef} className={styles.container}>
        <div className={styles.table}>
            <div className={styles.head}>
                <div className={styles.row}>
                    {fields && fields.map(({label, type}) => (
                        <div key={label} className={styles.th}>
                            {label}
                        </div>
                    ))}
                </div>
            </div>
            <div className={styles.body}>
                {rows && rows.map(({key, values}, index) => (
                    <div key={key} className={cn(
                        styles.row,
                        {
                            [styles.active]: activeRow === index
                        }
                    )}>
                        {fields && fields.map(({label, type, options}, fieldIndex) => (
                            <div key={label} className={styles.td}>
                                {type !== 'string' && (activeRow === index || ['checkbox', 'select', 'link'].indexOf(type as string) > -1) ? (
                                    <div className={cn(
                                        styles.edit,
                                        styles[`${type}Type`]
                                    )}>
                                        {type === 'input' ? (
                                            <input
                                                className={styles.input}
                                                value={values[label]}
                                                onChange={onFieldChange && onFieldChange(index, label) as any}
                                                onKeyPress={onKeyPress && onKeyPress as any}
                                            />
                                        ): undefined}
                                        {type === 'checkbox' ? (
                                            <input
                                                type="checkbox"
                                                className={styles.checkbox}
                                                checked={values[label] === true}
                                                onChange={onFieldChange && onFieldChange(index, label) as any}
                                            />
                                        ): undefined}
                                        {type === 'select' ? (
                                            <select
                                                className={styles.select}
                                                value={values[label]}
                                                onChange={onFieldChange && onFieldChange(index, label) as any}
                                            >
                                                {(options as any).options.map((item: string) => (
                                                    <option key={item} value={item}>
                                                        {item}
                                                    </option>
                                                ))}
                                            </select>
                                        ): undefined}
                                        {type === 'link' ? (
                                            <a
                                                className={styles.link}
                                                onClick={(options as any).onClick({history, url: (options as any).url})}
                                                href={(options as any).url}
                                            >
                                                {values[label]}
                                            </a>
                                        ): undefined}
                                    </div>
                                ) : (
                                    (
                                        <div onClick={onCellClick && onCellClick(index, fieldIndex)} className={styles.value}>
                                            {type === 'input' ? values[label] : undefined}
                                            {type === 'string' ? values[label] : undefined}
                                            {type === 'select' ? values[label] : undefined}
                                        </div>
                                    )
                                )}
                            </div>
                        ))}
                    </div>
                ))}
            </div>
        </div>
    </div>
)

const Table: React.ComponentClass<ITable> = compose<any, any>(
    withState('activeRow', 'setActiveRow', null),
    withProps((): IConnectedTable => ({
        containerRef: createRef(),
    })),
    withRouter,
    withProps(
        ({
             setActiveRow,
             onChangeField,
             containerRef,
            fields,
        }: IConnectedTable): IConnectedTable => ({
            onCellClick: (row: number, field: number) => () => {
                const fieldObject = fields && fields[field]

                if(
                    fieldObject
                    && (fieldObject.type) === 'checkbox'
                ) {
                    return
                }

                if(setActiveRow) {
                    setActiveRow(row)

                    if(containerRef && containerRef.current) {
                        const rowRef = containerRef
                            .current
                            .querySelector(`.${styles.row}:nth-child(${row+1})`)
                        if(rowRef) {
                            const cellRef = rowRef.querySelector(`.${styles.td}:nth-child(${field + 1})`)

                            if(cellRef) {
                                setTimeout(() => {
                                    const inputRef: HTMLInputElement = cellRef.querySelector(`.${styles.input}`) as HTMLInputElement

                                    if(inputRef) {
                                        inputRef.focus()
                                    }
                                })
                            }
                        }
                    }
                }
            },
            onFieldChange: (row: number, field: string) => ({target}:{target: HTMLInputElement} & Event) => {
                if(onChangeField) {
                    const value = {
                        checkbox: target.checked,
                        text: target.value,
                    }[target.type]

                    onChangeField(row, field, value)
                }
            },
            onKeyPress(event: KeyboardEvent): void {
                if(event.key === 'Enter' && setActiveRow) {
                    setActiveRow(null)
                }
            }
        })
    )
)(TableComponent)

export default Table