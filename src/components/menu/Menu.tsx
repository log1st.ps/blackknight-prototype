import cn from 'classnames'
import * as React from 'react'
import { ReactNode } from 'react'
import {
    matchPath,
    withRouter
} from 'react-router'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'

import styles from './menu.module.css'

const Menu = ({
    items = [
        'dashboard',
        'admins',
        'employees',
        'customers',
        'warranty',
        'sponsorship',
        'market',
        'accounting',
        'marketing',
        'clubs',
        'mailing',
        'inventory',
    ],
    onClick,
}: {
    items: string[],
    children?: ReactNode,
    onClick(key: string): () => void,
}) => (
    <div className={styles.menu}>
        {items.map(item => (
            <a
                key={item}
                className={cn(
                    styles.item,
                    {
                        [styles.active]: !!matchPath(location.pathname, `/${item}`)
                    }
                )}
                href={`/${item}`}
                onClick={onClick(`/${item}`)}
            >
                {item[0].toUpperCase() + item.slice(1, item.length)}
            </a>
        ))}
    </div>
)


export default compose(
    withRouter,
    withProps(({
        history
    }) => ({
        onClick: (key: string) => (event: Event) => {
            event.preventDefault()
            history.push(key)
        }
    }))
)(Menu)