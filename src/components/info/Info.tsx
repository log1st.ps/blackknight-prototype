import * as React from 'react'

import styles from './info.module.css'

interface IInfo {
    fields: Array<{
        key: string,
        value: string,
    }>
}

const Info = ({
    fields,
}: IInfo) => (
    <div>
        {fields.map(({key, value}) => (
            <div className={styles.row}>
                <div className={styles.field}>
                    {key}
                </div>
                <div className={styles.value}>
                    {value}
                </div>
            </div>
        ))}
    </div>
)

export default Info