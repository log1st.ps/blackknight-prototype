import cn from 'classnames'
import * as React from 'react'
import styles from './controls.module.css'

interface IControls {
    buttons?: Array<{
        key: string,
        label?: string,
        url?: string,
        isActive?: boolean,
        onClick?(): void,
    }>
}

const Controls = ({
    buttons,
}: IControls) => (
    <div className={styles.container}>
        {buttons && buttons.map(({key, label, url, onClick, isActive}) => {
            const Tag = (!!url ? 'a' : 'button') as any

            return (
                <div className={styles.buttonWrapper} key={key}>
                    <Tag
                        onClick={onClick}
                        url={url ? url : undefined}
                        className={cn(
                            styles.button,
                            {
                                [styles.active]: isActive,
                            }
                        )}
                    >
                        {label}
                    </Tag>
                </div>
            )
        })}
    </div>
)

export default Controls