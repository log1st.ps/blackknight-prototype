import * as React from 'react'

import { withRouter } from 'react-router'
import compose from'recompose/compose'
import withProps from'recompose/withProps'
import styles from './header.module.css'

const HeaderComponent = ({
    onClick,
}: any) => (
    <div className={styles.header}>
        <a className={styles.logo} onClick={onClick} href="/"/>
    </div>
)

const Header = compose(
    withRouter,
    withProps(
        ({history}) => ({
            onClick: (event: Event) => {
                event.preventDefault()
                history.push('/')
            }
        })
    )
)(HeaderComponent)

export default Header