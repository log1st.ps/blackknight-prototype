import * as React from 'react'
import { ReactNode } from 'react'
import {
    compose,
    withProps,
    withState
} from "recompose";

import * as faker from 'faker'
import {
    withRouter
} from 'react-router'
import Table, { ITable } from '../../components/table/Table'
import styles from './campaignsPage.module.css'

interface ICampaignsPage {
    title?: string,
    children?: ReactNode,
    rows?: ITable['rows'],
    history?: any,
    setRows?(rows: ITable['rows']): void,
    onFieldChange?(row: number, field: string, value: string): void,
}

const statuses = [
    'Active',
    'Disabled'
]

const fields = [
    {
        label: 'Name',
        type: 'input',
    },
    {
        label: 'Affiliate',
        type: 'input',
    },
    {
        label: 'Attracted customers',
        type: 'string'
    },
    {
        label: 'Introduced funds by related orders',
        type: 'string',
    },
    {
        label: 'Status',
        options: {
            options: statuses,
        },
        type: 'select',
    },
]

const defaultRows: ITable['rows'] = Array(20).fill(null).map((item: any, index: number) => ({
    key: String(index),
    values: {
        Affiliate: faker.name.findName(),
        'Attracted customers': faker.random.number(100),
        'Introduced funds by related orders': faker.finance.amount(0, 10000, 2, '$'),
        Name: faker.random.word(),
        Status: faker.random.arrayElement(statuses)
    },
}))

const CampaignsPage = compose(
    withState('rows', 'setRows', defaultRows),
    withProps((): ICampaignsPage => ({
        title: 'Marketing'
    })),
    withProps(({rows, setRows}: ICampaignsPage): ICampaignsPage => ({
        onFieldChange(row: number, field: string, value: string): void {
            if(setRows && rows) {
                rows[row].values[field] = value

                setRows(rows)
            }
        }
    })),
    withRouter,
)(
    ({
         title,
         rows,
         onFieldChange,
        history,
     }: ICampaignsPage) => (
        <div>
            <h1>{title}</h1>
            <button className={styles.add}>
                Add campaign
            </button>
            <div className={styles.table}>
                <Table
                    fields={fields}
                    rows={rows}
                    onChangeField={onFieldChange}
                />
            </div>
        </div>
    )
)

export default CampaignsPage