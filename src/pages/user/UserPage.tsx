import {
    compose,
    withProps,
} from 'recompose';

import * as faker from 'faker'
import { ReactNode } from 'react'
import * as React from 'react'
import Info from '../../components/info/Info'
import styles from './userPage.module.css'

interface IUserPage {
    name: string,
    children?: ReactNode,
}

const UserPage = compose(
    withProps(
        (): IUserPage => ({
            name: faker.name.findName(),
        })
    )
)(
    ({
        name
     }: IUserPage) => (
        <div className={styles.page}>
            <h1>{name}</h1>
            <div className={styles.info}>
                <h2 className={styles.subTitle}>User</h2>
                <Info fields={[
                    {key: 'First name', value: faker.name.firstName()},
                    {key: 'Last name', value: faker.name.lastName()},
                    {key: 'Email', value: faker.internet.email()},
                    {key: 'Phone', value: faker.phone.phoneNumber()},
                    {key: 'Role', value: faker.random.arrayElement(['Admin', 'Employee', 'Affiliate'])},
                ]}/>
            </div>
            <div className={styles.info}>
                <h2 className={styles.subTitle}>Orders</h2>
                <Info
                    fields={Array(faker.random.number({min: 1, max: 10})).fill(null).map(() =>
                        ({
                            key: `#${faker.random.number()}`,
                            value: faker.finance.amount(0, 10000, 2, '$')
                        }),
                    )}
                />
            </div>
        </div>
    )
)

export default UserPage