import * as React from 'react'
import { ReactNode } from 'react'
import {
    compose,
    withProps,
    withState
} from "recompose";

import * as faker from 'faker'
import {
    withRouter
} from 'react-router'
import Table, { ITable } from '../../components/table/Table'
import styles from './mailingPage.module.css'

interface ICampaignsPage {
    title?: string,
    children?: ReactNode,
    rows?: ITable['rows'],
    history?: any,
    setRows?(rows: ITable['rows']): void,
    onFieldChange?(row: number, field: string, value: string): void,
}

const fields = [
    {
        label: 'Name',
        type: 'input',
    },
    {
        label: 'Sent',
        type: 'string'
    },
    {
        label: 'Bounced',
        type: 'string'
    },
    {
        label: 'Attached users',
        type: 'string'
    }
]

const defaultRows: ITable['rows'] = Array(20).fill(null).map((item: any, index: number) => ({
    key: String(index),
    values: {
        'Attached users': faker.random.number(),
        Bounced: faker.random.number(),
        Name: faker.random.word(),
        Sent: faker.random.number(),
    },
}))

const MailingPage = compose(
    withState('rows', 'setRows', defaultRows),
    withProps((): ICampaignsPage => ({
        title: 'Mailing'
    })),
    withProps(({rows, setRows}: ICampaignsPage): ICampaignsPage => ({
        onFieldChange(row: number, field: string, value: string): void {
            if(setRows && rows) {
                rows[row].values[field] = value

                setRows(rows)
            }
        }
    })),
    withRouter,
)(
    ({
         title,
         rows,
         onFieldChange,
        history,
     }: ICampaignsPage) => (
        <div>
            <h1>{title}</h1>
            <button className={styles.add}>
                Add mail template
            </button>
            <div className={styles.table}>
                <Table
                    fields={fields}
                    rows={rows}
                    onChangeField={onFieldChange}
                />
            </div>
        </div>
    )
)

export default MailingPage