import { ReactNode } from 'react'
import * as React from 'react'
import { withRouter } from 'react-router'
import {
    compose,
    withProps,
} from "recompose";
import Controls from '../../components/controls/Controls'
import styles from './inventoryPage.module.css'

interface IMarketPage {
    title?: string,
    children?: ReactNode,
    history?: any,
}

const InventoryPage = compose(
    withProps(
        (): IMarketPage => ({
            title: 'Inventory'
        })
    ),
    withRouter,
)(
    ({
        title,
        history,
     }: IMarketPage) => (
        <div>
            <h1>{title}</h1>
            <div className={styles.controls}>
                <Controls
                    buttons={['west', 'east'].map((item) => ({
                        key: item,
                        label: 'BK ' + item[0].toUpperCase() + item.slice(1, item.length),
                        onClick: () => {
                            if(history) {
                                history.push(`/inventory/${item}`)
                            }
                        },
                        url: `/inventory/${item}`
                    }))}
                />
            </div>
        </div>
    )
)

export default InventoryPage