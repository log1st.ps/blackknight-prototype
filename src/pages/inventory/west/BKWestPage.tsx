import * as React from 'react'
import { ReactNode } from 'react'
import {
    compose,
    withProps,
    withState
} from "recompose";

import * as faker from 'faker'
import {
    matchPath,
    withRouter
} from 'react-router'
import Controls from '../../../components/controls/Controls'
import Table, { ITable } from '../../../components/table/Table'
import styles from './bkPage.module.css'

interface IOrdersPage {
    title?: string,
    children?: ReactNode,
    rows?: ITable['rows'],
    history?: any,
    setRows?(rows: ITable['rows']): void,
    onFieldChange?(row: number, field: string, value: string): void,
}

const fields = [
    {
        label: 'Products',
        type: 'string'
    },
    {
        label: 'Orders',
        type: 'string'
    },
    {
        label: 'Back orders',
        type: 'string'
    },
]

const defaultRows: ITable['rows'] = Array(20).fill(null).map((item: any, index: number) => ({
    key: String(index),
    values: {
        'Back orders': faker.random.number(100),
        Orders: faker.random.number(1000),
        Products: faker.random.number(10000),
    },
}))

const BKWestPage = compose(
    withState('rows', 'setRows', defaultRows),
    withProps((): IOrdersPage => ({
        title: 'BK West'
    })),
    withProps(({rows, setRows}: IOrdersPage): IOrdersPage => ({
        onFieldChange(row: number, field: string, value: string): void {
            if(setRows && rows) {
                rows[row].values[field] = value

                setRows(rows)
            }
        }
    })),
    withRouter,
)(
    ({
         title,
         rows,
         onFieldChange,
        history,
     }: IOrdersPage) => (
        <div>
            <h1>{title}</h1>
            <div className={styles.controls}>
                <Controls
                    buttons={['west', 'east'].map((item) => ({
                        isActive: !!matchPath(`/inventory/${item}`, location.pathname),
                        key: item,
                        label: 'BK ' + item[0].toUpperCase() + item.slice(1, item.length),
                        onClick: () => {
                            if(history) {
                                history.push(`/inventory/${item}`)
                            }
                        },
                        url: `/inventory/${item}`
                    }))}
                />
            </div>
            <div className={styles.table}>
                <Table
                    fields={fields}
                    rows={rows}
                    onChangeField={onFieldChange}
                />
            </div>
        </div>
    )
)

export default BKWestPage