import * as React from 'react'

import cn from 'classnames'
import {
    Chart,
    chart as lineChart
} from 'highcharts'
import {
    createRef,
    RefObject
} from 'react'
import {
    compose,
    lifecycle,
    withProps
} from 'recompose';
import configs from './exampleCharts'
import styles from './indexPage.module.css'

const IndexPageComponent = ({
    widgetsRef
}: any) => (
    <div>
        <h1>Dashboard</h1>

        <div ref={widgetsRef} className={styles.widgets}>
            {Array(6).fill(null).map((item, index) => (
                <div key={index} className={styles.widget}>
                    <h3>Example widget #{index}</h3>
                    {index <= 4 ? (
                        <div className={styles.chart}/>
                    ): (
                        <div className={cn(
                            styles.chart,
                            styles.table
                        )}>
                            <table>
                                <thead>
                                <tr>
                                    {Array(5).fill(null).map((hm: any, field: any) => (
                                        <th key={field}>Field #{field}</th>
                                    ))}
                                </tr>
                                </thead>
                                <tbody>
                                    {Array(7).fill(null).map((hm: any, row: any) => (
                                        <tr key={row}>
                                            {Array(5).fill(null).map((hm2: any, value: any) => (
                                                <td key={value}>Field #{value}</td>
                                            ))}
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    )}
                </div>
            ))}
        </div>
    </div>
)

let charts: Chart[] = []

const IndexPage = compose(
    withProps(() => ({
        widgetsRef: createRef()
    })),
    lifecycle({
        componentDidMount() {
            const {widgetsRef} = this.props as {widgetsRef: RefObject<HTMLDivElement>}

            if(widgetsRef.current) {
                widgetsRef.current.querySelectorAll(`.${styles.chart}`).forEach((chart: HTMLDivElement, index) => {
                    if(index <= 4 && configs[index]) {
                        charts.push(lineChart(chart, configs[index] as any) as any)
                    }
                })
            }
        },
        componentWillUnmount() {
            charts.map(chart => {
                chart.destroy()
            })
            charts = [];
        }
    })
)(IndexPageComponent)

export default IndexPage