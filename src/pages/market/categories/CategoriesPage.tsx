import * as React from 'react'
import { ReactNode } from 'react'
import {
    compose,
    withProps,
    withState
} from "recompose";

import * as faker from 'faker'
import {
    matchPath,
    withRouter
} from 'react-router'
import Controls from '../../../components/controls/Controls'
import Table, { ITable } from '../../../components/table/Table'
import styles from './categoriesPage.module.css'

interface ICategoriesPage {
    title?: string,
    children?: ReactNode,
    rows?: ITable['rows'],
    history?: any,
    setRows?(rows: ITable['rows']): void,
    onFieldChange?(row: number, field: string, value: string): void,
}

const fields = [
    {
        label: 'Name',
        type: 'input',
    },
]

const defaultRows: ITable['rows'] = Array(20).fill(null).map((item: any, index: number) => ({
    key: String(index),
    values: {
        Name: faker.random.word(),
    },
}))

const CategoriesPage = compose(
    withState('rows', 'setRows', defaultRows),
    withProps((): ICategoriesPage => ({
        title: 'Categories'
    })),
    withProps(({rows, setRows}: ICategoriesPage): ICategoriesPage => ({
        onFieldChange(row: number, field: string, value: string): void {
            if(setRows && rows) {
                rows[row].values[field] = value

                setRows(rows)
            }
        }
    })),
    withRouter,
)(
    ({
         title,
         rows,
         onFieldChange,
        history,
     }: ICategoriesPage) => (
        <div>
            <h1>{title}</h1>
            <div className={styles.controls}>
                <Controls
                    buttons={['categories', 'products', 'orders', 'distributors'].map((item) => ({
                        isActive: !!matchPath(`/market/${item}`, location.pathname),
                        key: item,
                        label: item[0].toUpperCase() + item.slice(1, item.length),
                        onClick: () => {
                            if(history) {
                                history.push(`/market/${item}`)
                            }
                        },
                        url: `/market/${item}`
                    }))}
                />
            </div>
            <button className={styles.add}>
                Add category
            </button>
            <div className={styles.table}>
                <Table
                    fields={fields}
                    rows={rows}
                    onChangeField={onFieldChange}
                />
            </div>
        </div>
    )
)

export default CategoriesPage