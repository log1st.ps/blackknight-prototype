import { ReactNode } from 'react'
import * as React from 'react'
import { withRouter } from 'react-router'
import {
    compose,
    withProps,
} from "recompose";
import Controls from '../../components/controls/Controls'
import styles from './marketPage.module.css'

interface IMarketPage {
    title?: string,
    children?: ReactNode,
    history?: any,
}

const MarketPage = compose(
    withProps(
        (): IMarketPage => ({
            title: 'Market'
        })
    ),
    withRouter,
)(
    ({
        title,
        history,
     }: IMarketPage) => (
        <div>
            <h1>{title}</h1>
            <div className={styles.controls}>
                <Controls
                    buttons={['categories', 'products', 'orders', 'distributors'].map((item) => ({
                        key: item,
                        label: item[0].toUpperCase() + item.slice(1, item.length),
                        onClick: () => {
                            if(history) {
                                history.push(`/market/${item}`)
                            }
                        },
                        url: `/market/${item}`
                    }))}
                />
            </div>
        </div>
    )
)

export default MarketPage