import * as React from 'react'
import { ReactNode } from 'react'
import {
    compose,
    withProps,
    withState
} from "recompose";

import * as faker from 'faker'
import {
    matchPath,
    withRouter
} from 'react-router'
import Controls from '../../../components/controls/Controls'
import Table, { ITable } from '../../../components/table/Table'
import styles from './ordersPage.module.css'

interface IOrdersPage {
    title?: string,
    children?: ReactNode,
    rows?: ITable['rows'],
    history?: any,
    setRows?(rows: ITable['rows']): void,
    onFieldChange?(row: number, field: string, value: string): void,
}

const statuses = [
    'New',
    'In progress',
    'Paid',
    'Declined',
    'Done'
]

const fields = [
    {
        label: 'Customer',
        type: 'string',
    },
    {
        label: 'Count of goods',
        type: 'string',
    },
    {
        label: 'Price',
        type: 'string',
    },
    {
        label: 'Status',
        options: {
            options: statuses
        },
        type: 'select'
    },
    {
        label: 'Back order',
        type: 'string'
    }
]

const defaultRows: ITable['rows'] = Array(20).fill(null).map((item: any, index: number) => ({
    key: String(index),
    values: {
        'Back Order': faker.random.arrayElement(['Yes', 'No']),
        'Count of goods': faker.random.number({min: 1, max: 10}),
        Customer: faker.name.findName(),
        'Price': faker.finance.amount(100, 10000, 2, '$'),
        Status: faker.random.arrayElement(statuses)
    },
}))

const OrdersPage = compose(
    withState('rows', 'setRows', defaultRows),
    withProps((): IOrdersPage => ({
        title: 'Orders'
    })),
    withProps(({rows, setRows}: IOrdersPage): IOrdersPage => ({
        onFieldChange(row: number, field: string, value: string): void {
            if(setRows && rows) {
                rows[row].values[field] = value

                setRows(rows)
            }
        }
    })),
    withRouter,
)(
    ({
         title,
         rows,
         onFieldChange,
        history,
     }: IOrdersPage) => (
        <div>
            <h1>{title}</h1>
            <div className={styles.controls}>
                <Controls
                    buttons={['categories', 'products', 'orders', 'distributors'].map((item) => ({
                        isActive: !!matchPath(`/market/${item}`, location.pathname),
                        key: item,
                        label: item[0].toUpperCase() + item.slice(1, item.length),
                        onClick: () => {
                            if(history) {
                                history.push(`/market/${item}`)
                            }
                        },
                        url: `/market/${item}`
                    }))}
                />
            </div>
            <div className={styles.table}>
                <Table
                    fields={fields}
                    rows={rows}
                    onChangeField={onFieldChange}
                />
            </div>
        </div>
    )
)

export default OrdersPage