import * as React from 'react'
import { ReactNode } from 'react'
import {
    compose,
    withProps,
    withState
} from "recompose";

import * as faker from 'faker'
import Table, { ITable } from '../../components/table/Table'
import styles from './warrantyPage.module.css'

interface IUsersPage {
    title?: string,
    children?: ReactNode,
    rows?: ITable['rows'],
    setRows?(rows: ITable['rows']): void,
    onFieldChange?(row: number, field: string, value: string): void,
}

const fields = [
    {
        label: 'Id',
        options: {
            onClick: ({history, url}: any) => (event: Event) => {
                event.preventDefault()
                history.push(url)
            },
            url: `/users/view`
        },
        type: 'link'
    },
    {
        label: 'First name',
        type: 'input',
    },
    {
        label: 'Last name',
        type: 'input',
    },
    {
        label: 'Email',
        type: 'input',
    },
    {
        label: 'Phone',
        type: 'input',
    },
    {
        label: 'Is Active',
        type: 'checkbox',
    },
]

const defaultRows: ITable['rows'] = Array(20).fill(null).map((item: any, index: number) => ({
    key: String(index),
    values: {
        Email: faker.internet.email(),
        'First name': faker.name.firstName(),
        Id: faker.random.number(),
        'Is Active': faker.random.boolean(),
        'Last name': faker.name.lastName(),
        Phone: faker.phone.phoneNumber(),
    },
}))

const WarrantyPage = compose(
    withState('rows', 'setRows', defaultRows),
    withProps((): IUsersPage => ({
        title: 'Warranty'
    })),
    withProps(({rows, setRows}: IUsersPage): IUsersPage => ({
        onFieldChange(row: number, field: string, value: string): void {
            if(setRows && rows) {
                rows[row].values[field] = value

                setRows(rows)
            }
        }
    }))
)(
    ({
        title,
        rows,
        onFieldChange
     }: IUsersPage) => (
        <div>
            <h1>{title}</h1>
            <div className={styles.table}>
                <Table
                    fields={fields}
                    rows={rows}
                    onChangeField={onFieldChange}
                />
            </div>
        </div>
    )
)

export default WarrantyPage