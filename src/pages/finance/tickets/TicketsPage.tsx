import * as React from 'react'
import { ReactNode } from 'react'
import {
    compose,
    withProps,
    withState
} from "recompose";

import * as faker from 'faker'
import {
    matchPath,
    withRouter
} from 'react-router'
import Controls from '../../../components/controls/Controls'
import Table, { ITable } from '../../../components/table/Table'
import styles from './ticketsPage.module.css'

interface ITicketsPage {
    title?: string,
    children?: ReactNode,
    rows?: ITable['rows'],
    history?: any,
    setRows?(rows: ITable['rows']): void,
    onFieldChange?(row: number, field: string, value: string): void,
}

const statuses = [
    'New',
    'Pending',
    'Declined',
    'Approved',
]

const fields = [
    {
        label: 'Customer',
        type: 'string',
    },
    {
        label: 'Amount',
        type: 'string',
    },
    {
        label: 'Type',
        type: 'string'
    },
    {
        label: 'Employee',
        type: 'string',
    },
    {
        label: 'Status',
        options: {
            options: statuses,
        },
        type: 'select'
    },
]

const defaultRows: ITable['rows'] = Array(20).fill(null).map((item: any, index: number) => ({
    key: String(index),
    values: {
        Amount: faker.finance.amount(0, 10000, 2, '$'),
        Customer: faker.name.findName(),
        Employee: faker.name.findName(),
        Status: faker.random.arrayElement(statuses),
        Type: faker.random.arrayElement(['Deposit', 'Withdrawal'])
    },
}))

const TicketsPage = compose(
    withState('rows', 'setRows', defaultRows),
    withProps((): ITicketsPage => ({
        title: 'Purchase requests'
    })),
    withProps(({rows, setRows}: ITicketsPage): ITicketsPage => ({
        onFieldChange(row: number, field: string, value: string): void {
            if(setRows && rows) {
                rows[row].values[field] = value

                setRows(rows)
            }
        }
    })),
    withRouter,
)(
    ({
         title,
         rows,
         onFieldChange,
        history,
     }: ITicketsPage) => (
        <div>
            <h1>{title}</h1>
            <div className={styles.controls}>
                <Controls
                    buttons={['tickets', 'transactions'].map((item) => ({
                        isActive: !!matchPath(`/accounting/${item}`, location.pathname),
                        key: item,
                        label: {
                            tickets: 'Purchase requests',
                            transactions: 'Completed'
                        }[item],
                        onClick: () => {
                            if(history) {
                                history.push(`/accounting/${item}`)
                            }
                        },
                        url: `/accounting/${item}`
                    }))}
                />
            </div>
            <button className={styles.add}>
                Add ticket
            </button>
            <div className={styles.table}>
                <Table
                    fields={fields}
                    rows={rows}
                    onChangeField={onFieldChange}
                />
            </div>
        </div>
    )
)

export default TicketsPage