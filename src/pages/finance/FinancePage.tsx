import { ReactNode } from 'react'
import * as React from 'react'
import { withRouter } from 'react-router'
import {
    compose,
    withProps,
} from "recompose";
import Controls from '../../components/controls/Controls'
import styles from './financePage.module.css'

interface IFinancePage {
    title?: string,
    children?: ReactNode,
    history?: any,
}

const FinancePage = compose(
    withProps(
        (): IFinancePage => ({
            title: 'Accounting'
        })
    ),
    withRouter,
)(
    ({
         title,
         history,
     }: IFinancePage) => (
        <div>
            <h1>{title}</h1>
            <div className={styles.controls}>
                <Controls
                    buttons={['tickets', 'transactions'].map((item) => ({
                        key: item,
                        label: {
                            tickets: 'Purchase requests',
                            transactions: 'Completed'
                        }[item],
                        onClick: () => {
                            if(history) {
                                history.push(`/accounting/${item}`)
                            }
                        },
                        url: `/accounting/${item}`
                    }))}
                />
            </div>
        </div>
    )
)

export default FinancePage