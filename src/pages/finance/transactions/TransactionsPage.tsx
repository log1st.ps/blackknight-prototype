import * as React from 'react'
import { ReactNode } from 'react'
import {
    compose,
    withProps,
    withState
} from "recompose";

import * as faker from 'faker'
import {
    matchPath,
    withRouter
} from 'react-router'
import Controls from '../../../components/controls/Controls'
import Table, { ITable } from '../../../components/table/Table'
import styles from './transactionsPage.module.css'

interface ITransactionsPage {
    title?: string,
    children?: ReactNode,
    rows?: ITable['rows'],
    history?: any,
    setRows?(rows: ITable['rows']): void,
    onFieldChange?(row: number, field: string, value: string): void,
}

const fields = [
    {
        label: 'Customer',
        type: 'string',
    },
    {
        label: 'Amount Before',
        type: 'string',
    },
    {
        label: 'Amount',
        type: 'string',
    },
    {
        label: 'Type',
        type: 'string'
    },
    {
        label: 'Employee',
        type: 'string',
    },
]

const defaultRows: ITable['rows'] = Array(20).fill(null).map((item: any, index: number) => ({
    key: String(index),
    values: {
        Amount: faker.finance.amount(0, 10000, 2, '$'),
        'Amount Before': faker.finance.amount(0, 10000, 2, '$'),
        Customer: faker.name.findName(),
        Employee: faker.name.findName(),
        Type: faker.random.arrayElement(['Deposit', 'Withdrawal'])
    },
}))

const TransactionsPage = compose(
    withState('rows', 'setRows', defaultRows),
    withProps((): ITransactionsPage => ({
        title: 'Completed purchases'
    })),
    withProps(({rows, setRows}: ITransactionsPage): ITransactionsPage => ({
        onFieldChange(row: number, field: string, value: string): void {
            if(setRows && rows) {
                rows[row].values[field] = value

                setRows(rows)
            }
        }
    })),
    withRouter,
)(
    ({
         title,
         rows,
         onFieldChange,
        history,
     }: ITransactionsPage) => (
        <div>
            <h1>{title}</h1>
            <div className={styles.controls}>
                <Controls
                    buttons={['tickets', 'transactions'].map((item) => ({
                        isActive: !!matchPath(`/accounting/${item}`, location.pathname),
                        key: item,
                        label: {
                            tickets: 'Purchase requests',
                            transactions: 'Completed'
                        }[item],
                        onClick: () => {
                            if(history) {
                                history.push(`/accounting/${item}`)
                            }
                        },
                        url: `/accounting/${item}`
                    }))}
                />
            </div>
            <button className={styles.add}>
                Add manual transaction
            </button>
            <div className={styles.table}>
                <Table
                    fields={fields}
                    rows={rows}
                    onChangeField={onFieldChange}
                />
            </div>
        </div>
    )
)

export default TransactionsPage