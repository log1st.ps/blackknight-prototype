import * as React from 'react'
import { ReactNode } from 'react'
import {
    compose,
    withProps,
    withState
} from "recompose";

import * as faker from 'faker'
import Table, { ITable } from '../../components/table/Table'
import styles from './clubsPage.module.css'

interface IUsersPage {
    title?: string,
    children?: ReactNode,
    rows?: ITable['rows'],
    setRows?(rows: ITable['rows']): void,
    onFieldChange?(row: number, field: string, value: string): void,
}

const fields = [
    {
        label: 'Name',
        type: 'input',
    },
    {
        label: 'Members Count',
        type: 'input',
    },
    {
        label: 'Is Active',
        type: 'checkbox',
    },
]

const defaultRows: ITable['rows'] = Array(20).fill(null).map((item: any, index: number) => ({
    key: String(index),
    values: {
        'Is Active': faker.random.boolean(),
        'Members Count': faker.random.number(100),
        'Name': faker.random.word(),
    },
}))

const ClubsPage = compose(
    withState('rows', 'setRows', defaultRows),
    withProps((): IUsersPage => ({
        title: 'Clubs'
    })),
    withProps(({rows, setRows}: IUsersPage): IUsersPage => ({
        onFieldChange(row: number, field: string, value: string): void {
            if(setRows && rows) {
                rows[row].values[field] = value

                setRows(rows)
            }
        }
    }))
)(
    ({
        title,
        rows,
        onFieldChange
     }: IUsersPage) => (
        <div>
            <h1>{title}</h1>
            <div className={styles.table}>
                <Table
                    fields={fields}
                    rows={rows}
                    onChangeField={onFieldChange}
                />
            </div>
        </div>
    )
)

export default ClubsPage