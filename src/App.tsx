import * as React from 'react'
import {
    BrowserRouter,
    Redirect,
    Route
} from 'react-router-dom'
import Header from './components/header/Header'
import Menu from './components/menu/Menu'

import styles from './App.module.css'
import AdminsPage from './pages/admins/AdminsPage'
import CampaignsPage from './pages/campaigns/CampaignsPage'
import ClubsPage from './pages/clubs/ClubsPage'
import CustomersPage from './pages/customers/CustomersPage'
import EmployeesPage from './pages/employees/EmployeesPage'
import FinancePage from './pages/finance/FinancePage'
import TicketsPage from './pages/finance/tickets/TicketsPage'
import TransactionsPage from './pages/finance/transactions/TransactionsPage'
import IndexPage from './pages/index/IndexPage'
import BKEastPage from './pages/inventory/east/BKEastPage'
import InventoryPage from './pages/inventory/InventoryPage'
import BKWestPage from './pages/inventory/west/BKWestPage'
import MailingPage from './pages/mailing/MailingPage'
import CategoriesPage from './pages/market/categories/CategoriesPage'
import DistributorsPage from './pages/market/distributors/DistributorsPage'
import GoodsPage from './pages/market/goods/GoodsPage'
import MarketPage from './pages/market/MarketPage'
import OrdersPage from './pages/market/orders/OrdersPage'
import SponsorshipPage from './pages/sponsorship/SponsorshipPage'
import UserPage from './pages/user/UserPage'
import WarrantyPage from './pages/warranty/WarrantyPage'

const RedirectToDashboard = () => <Redirect to={'/dashboard'}/>

const App = ({

}) => (
    <div className={styles.app}>
        <BrowserRouter>
            <div className={styles.layout}>
                <div className={styles.header}>
                    <Header/>
                </div>
                <div className={styles.container}>
                    <div className={styles.menu}>
                        <Menu/>
                    </div>
                    <div className={styles.content}>
                        <Route exact={true} path={'/'} render={RedirectToDashboard}/>
                        <Route exact={true} path={'/dashboard'} component={IndexPage}/>
                        <Route exact={true} path={'/users/view'} component={UserPage}/>
                        <Route exact={true} path={'/admins'} component={AdminsPage}/>
                        <Route exact={true} path={'/customers'} component={CustomersPage}/>
                        <Route exact={true} path={'/employees'} component={EmployeesPage}/>
                        <Route exact={true} path={'/warranty'} component={WarrantyPage}/>
                        <Route exact={true} path={'/sponsorship'} component={SponsorshipPage}/>
                        <Route exact={true} path={'/market'} component={MarketPage}/>
                        <Route exact={true} path={'/market/categories'} component={CategoriesPage}/>
                        <Route exact={true} path={'/market/orders'} component={OrdersPage}/>
                        <Route exact={true} path={'/market/distributors'} component={DistributorsPage}/>
                        <Route exact={true} path={'/market/products'} component={GoodsPage}/>
                        <Route exact={true} path={'/accounting'} component={FinancePage}/>
                        <Route exact={true} path={'/accounting/tickets'} component={TicketsPage}/>
                        <Route exact={true} path={'/accounting/transactions'} component={TransactionsPage}/>
                        <Route exact={true} path={'/marketing'} component={CampaignsPage}/>
                        <Route exact={true} path={'/clubs'} component={ClubsPage}/>
                        <Route exact={true} path={'/mailing'} component={MailingPage}/>
                        <Route exact={true} path={'/inventory'} component={InventoryPage}/>
                        <Route exact={true} path={'/inventory/west'} component={BKWestPage}/>
                        <Route exact={true} path={'/inventory/east'} component={BKEastPage}/>
                    </div>
                </div>
            </div>
        </BrowserRouter>
    </div>
)

export default App